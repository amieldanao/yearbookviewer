package com.example.amiel.yearbookviewer;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.math.MathUtils;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public int CHOOSE_FILE_REQUESTCODE = 1;
    public final int MY_PERMISSIONS_REQUEST_READ_FILE = 2;

    private CurlView mCurlView;

    private PageProvider myPageProvider;

    private MenuItem pageSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        CREATE();

    }

    public void CREATE(){
        int index = 0;
        if (getLastNonConfigurationInstance() != null) {
            index = (Integer) getLastNonConfigurationInstance();
        }
        mCurlView = findViewById(R.id.curl);
        mCurlView.setSizeChangedObserver(new SizeChangedObserver());
        mCurlView.setCurrentIndex(0);
        mCurlView.setBackgroundColor(16777190);

        showTempDialog();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        pageSelect = (MenuItem) menu.findItem(R.id.action_page);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //show page select
        if (id == R.id.action_page) {
            showNumericInput();



            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void gotoPage(int pageNumber){
        mCurlView.setCurrentIndex(pageNumber);
    }

    private void showNumericInput()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Input Page Number");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        alert.setView(input);
        alert.setPositiveButton("Go", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for OK button here
                int selected = Integer.parseInt(input.getText().toString())-1;
                if (selected > myPageProvider.getPageCount() || selected < 0) {
                    msgBox("Page does not exist!", "Warning", "Close");
                } else {
                    gotoPage(selected);
                }
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //Put actions for CANCEL button here, or leave in blank
            }
        });
        alert.show();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_open) {

            checkPermission();


        } else if (id == R.id.nav_recent) {

        } else if (id == R.id.nav_about) {
            msgBox("NCST Year Book 2018", "About", "close");
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void msgBox(String text, String title, String btnText){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(text);
        dlgAlert.setTitle(title);
        //dlgAlert.setPositiveButton("OK", null);


        dlgAlert.setPositiveButton(btnText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }


    public void openFile(String minmeType) {

        //Intent intent = getIntent();
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        //intent.setDataAndType(android.net.Uri.parse(".*\\\\.yrbm"), minmeType);
        //intent.setType(minmeType);
        /*Uri privateUri = Uri.parse(".*\\\\.yrbm");
        Intent intent = new Intent(getApplicationContext(), this.getClass());
        intent.putExtra("DATA_URI", privateUri);*/

        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", minmeType);
        //sIntent.putExtra("CONTENT_TYPE", intent.getType());
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        }
        else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, CHOOSE_FILE_REQUESTCODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        showTempDialog();
        if (data != null) {
            String Fpath = data.getDataString();

            //check if the file extension is supported
            Log.d("TAG", Fpath.substring(Fpath.length()-5, Fpath.length()));
            if (Fpath.substring(Fpath.length()-5, Fpath.length()).equals(".yrbm")) {

                // do somthing...
                super.onActivityResult(requestCode, resultCode, data);

                String newPath = Fpath.substring(8, Fpath.length());

                //Toast.makeText(getApplicationContext(), readFromFile(getApplicationContext(), newPath), Toast.LENGTH_SHORT).show();

                setMyStage(readFromFile(getApplicationContext(), newPath));
            }
            else
            {
                msgBox("File type not supported! ", "Warning", "Close");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        mCurlView.onResume();





        showTempDialog();
    }

    @Override
    public void onRestart() {

        super.onRestart();

        showTempDialog();

    }

    @Override
    public void onPause() {

        super.onPause();
        mCurlView.onPause();

        showTempDialog();
    }

    public void setMyStage(String str){
        myPageProvider = new PageProvider();
        myPageProvider.mBitmapIds = new ArrayList<>();

        Pattern p2 = Pattern.compile("\\[(.*?)\\]");
        Matcher m2 = p2.matcher(str);
        while (m2.find()) {
            //logLargeString( m2.group(1));
            //add pages
            try {
                ImageView myImage = new ImageView(this);
                //ImageView myImage = findViewById(R.id.imageView3);
                //myImage.setImageResource(R.drawable.ic_launcher);
                byte[] decodedString = Base64.decode(m2.group(1), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                myImage.setImageBitmap(decodedByte);
                //myImage.setImageResource(R.drawable.ic_menu_camera);

                ConstraintLayout.LayoutParams clpcontactUs = new ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.MATCH_CONSTRAINT);
                myImage.setLayoutParams(clpcontactUs);


                Drawable d = new BitmapDrawable(getResources(), decodedByte);

                myPageProvider.mBitmapIds.add(d);

            } catch (IllegalArgumentException e) {
                // TODO: handle exception
                Log.d("TAG", e.getMessage());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        //}


        mCurlView.setPageProvider(myPageProvider);


        pageSelect.setEnabled(true);



        showTempDialog();
    }

    private void showTempDialog(){
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);

        View myView = findViewById(R.id.nav_view);
        myView.bringToFront();
        myView.getParent().requestLayout();
    }

    private String readFromFile(Context context,String path) {

        String ret = "";

        try {
            //InputStream inputStream = context.openFileInput(path);

            FileInputStream inputStream = new FileInputStream (new File(path));

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("TAG", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("TAG", "Can not read file: " + e.toString());
        }

        return ret;
    }


    public void checkPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_FILE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            openFile("*/*");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_FILE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openFile("*/*");
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }





    private class PageProvider implements CurlView.PageProvider {

        // Bitmap resources.
       /* public int[] mBitmapIds = { R.drawable.obama, R.drawable.road_rage,
                R.drawable.taipei_101, R.drawable.world };*/
       public List<Drawable> mBitmapIds = new ArrayList<>();;

        @Override
        public int getPageCount() {
            return mBitmapIds.size();
        }


        private Bitmap loadBitmap(int width, int height, int index) {
            Bitmap b = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);
            b.eraseColor(0xFFFFFFFF);
            Canvas c = new Canvas(b);
            Drawable d = mBitmapIds.get(index);//getResources().getDrawable(mBitmapIds.get(index));

            int margin = 7;
            int border = 3;
            Rect r = new Rect(margin, margin, width - margin, height - margin);

            int imageWidth = r.width() - (border * 2);
            int imageHeight = imageWidth * d.getIntrinsicHeight()
                    / d.getIntrinsicWidth();
            if (imageHeight > r.height() - (border * 2)) {
                imageHeight = r.height() - (border * 2);
                imageWidth = imageHeight * d.getIntrinsicWidth()
                        / d.getIntrinsicHeight();
            }

            r.left += ((r.width() - imageWidth) / 2) - border;
            r.right = r.left + imageWidth + border + border;
            r.top += ((r.height() - imageHeight) / 2) - border;
            r.bottom = r.top + imageHeight + border + border;

            Paint p = new Paint();
            p.setColor(0xFFC0C0C0);
            c.drawRect(r, p);
            r.left += border;
            r.right -= border;
            r.top += border;
            r.bottom -= border;

            d.setBounds(r);
            d.draw(c);

            return b;
        }

        //@Override
        public void updatePage(CurlPage page, int width, int height, int index) {
        //public void UPDATE(CurlPage page, int width, int height, int index) {
            //index = 2;
            //updatePage(page, width, height, index);

                Log.d("TAG", "PAGE : " + index + " out of " + (myPageProvider.getPageCount()-1));
          //


              Bitmap lront = loadBitmap(width, height, Math.min(index, myPageProvider.getPageCount() - 1));

              page.setTexture(lront, CurlPage.SIDE_BOTH);
              page.setColor(Color.argb(127, 255, 255, 255),
                      CurlPage.SIDE_BACK);


            if (false) {

                switch (index) {
                    // First case is image on front side, solid colored back.
                    case 0: {
                        Bitmap front = loadBitmap(width, height, 0);
                        page.setTexture(front, CurlPage.SIDE_FRONT);
                        page.setColor(Color.rgb(180, 180, 180), CurlPage.SIDE_BACK);
                        break;
                    }
                    // Second case is image on back side, solid colored front.
                    case 1: {
                        Bitmap back = loadBitmap(width, height, 2);
                        page.setTexture(back, CurlPage.SIDE_BACK);
                        page.setColor(Color.rgb(127, 140, 180), CurlPage.SIDE_FRONT);
                        break;
                    }
                    // Third case is images on both sides.
                    case 2: {
                        Bitmap front = loadBitmap(width, height, 1);
                        Bitmap back = loadBitmap(width, height, 3);
                        page.setTexture(front, CurlPage.SIDE_FRONT);
                        page.setTexture(back, CurlPage.SIDE_BACK);
                        break;
                    }
                    // Fourth case is images on both sides - plus they are blend against
                    // separate colors.
                    case 3: {
                        Bitmap front = loadBitmap(width, height, 2);
                        Bitmap back = loadBitmap(width, height, 1);
                        page.setTexture(front, CurlPage.SIDE_FRONT);
                        page.setTexture(back, CurlPage.SIDE_BACK);
                        page.setColor(Color.argb(127, 170, 130, 255),
                                CurlPage.SIDE_FRONT);
                        page.setColor(Color.rgb(255, 190, 150), CurlPage.SIDE_BACK);
                        break;
                    }
                    // Fifth case is same image is assigned to front and back. In this
                    // scenario only one texture is used and shared for both sides.
                    case 4:
                        Bitmap front = loadBitmap(width, height, 0);
                        page.setTexture(front, CurlPage.SIDE_BOTH);
                        page.setColor(Color.argb(127, 255, 255, 255),
                                CurlPage.SIDE_BACK);
                        break;
                }
            }
        }

    }

    /**
     * CurlView size changed observer.
     */
    private class SizeChangedObserver implements CurlView.SizeChangedObserver {
        @Override
        public void onSizeChanged(int w, int h) {
            if (w > h) {
                mCurlView.setViewMode(CurlView.SHOW_TWO_PAGES);
                mCurlView.setMargins(.1f, .05f, .1f, .05f);
            } else {
                mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
                mCurlView.setMargins(.01f, .01f, .01f, .01f);
            }
        }
    }






}